# Steps to recreate error in PowerRuby v1.0.9

Create a new Rails app

```
rails new app1 --skip-bundle
cd app1
```

Update `Gemfile` to have `tzinfo-data`, otherwise it will error on subsequent `rails` commands.

```
echo "gem 'tzinfo-data'" >> Gemfile
```
Resolve gems and generate `Gemfile.lock`
```
bundle install --local
```
Paste the following block into the shell to generate 400 tables; enough to re-recreate the issue.
``` 
for i in {1..400}
do
   rails g scaffold screen$i fld1:text fld2:decimal fld3:boolean fld4:text fld5:decimal fld6:boolean fld7:text fld8:decimal fld9:boolean fld10:text fld11:decimal fld12:boolean
done
```
Run migrations.  Note this is using sqlite3 and not DB2 for i.
```
rake db:migrate RAILS_ENV=test
```
Run the tests.  
```
rake test
```

This should create an error similar to `illegal hardware instruction (core dumped)`.